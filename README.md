Projeto Send Email
==============================

Código fonte da aplicação send Email.
Essa aplicação é uma API em node.JS para envio de e-mails.

Tecnologias utilizadas
-------------

Esta aplicação consiste em uma série de APIs construídas em __NodeJS__ utilizando o banco de dados __MongoDB__.

Configuração
-------------

__Pré requisitos:__
Para o desenvolvimento neste projeto é necessário ter os seguintes itens instalados e configurados:

* [NodeJS](https://nodejs.org/);
* [MongoDB](https://www.mongodb.com/);
* [Git](https://git-scm.com/);
* [TortoiseGit](https://tortoisegit.org/) *opcional;
* [Atom](https://atom.io/) *ou qualquer outro editor de textos avançado;

__Iniciando o projeto__

* Clone o repositório do Git na pasta desejada utilizando o comando `git clone https://SEU_USUARIO@bitbucket.org/Biamoura/sendemail.git`;
* Para cada um dos projetos, identificados por um arquivo *package.json* dentro de suas pastas, execute o comando `npm install`
* Para iniciar a API execute o comando `node` em sua respectiva pasta


__Qualidade de código__

Para que o projeto possua uma boa qualidade de código fonte sempre deve ser utilizado o [JSCS](http://jscs.info/) que é uma ferramenta de validação e formatação de código.

>O JSCS pode ser configurado tanto no NodeJS quanto nos editores de texto mais avançados por meio de plugins e recomenda-se a utilização das duas formas em conjunto.

_Configurando e exeurando o JSCS no NodeJS:_

* Baixar: `npm install jscs -g`
* Verificar arquivos na pasta: `jscs .`
* Verificar e ajustar arquivos na pasta: `jscs NOME_DO_ARQUIVO --fix`
* Verificar um único arquivo: `jscs NOME_DO_ARQUIVO`
* Verificar e ajustar um único arquivo: `jscs NOME_DO_ARQUIVO --fix`