const config = require ('./config/config.json');
const server = require ('./config/server');
const emailRoute = require('./src/routes/email_route')(server);

server.listen(config.server.port, function() {
  console.log('Servidor ON');
});
