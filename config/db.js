const config = require('../config/config');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect(config.db.uri);

mongoose.connection.on('connected', function() {
  console.log('Mongoose connection open to ' + config.db.uri);
});

mongoose.connection.on('error',function(err) {
  console.log('Mongoose connection error: ' + err);
});

mongoose.connection.on('disconnected', function() {
  console.log('Mongoose connection disconnected');
});

mongoose.connection.on('open', function() {
  console.log('Mongoose connection is open');
});

process.on('SIGINT', function() {
  mongoose.connection.close(function() {
    console.log('Mongoose connection disconnected through app termination');
    process.exit(0);
  });
});
