const request = require('request');
const jwt = require('jsonwebtoken');
const config = require('./config');

module.exports.checkToken = function(req, res, next) {
  const authorizationHeader = req.headers['authorization'];
  if (authorizationHeader) {
    const token = authorizationHeader.split(' ')[1];
    jwt.verify(
      token,
      config.security.tokenKey,
      function(err, decoded) {
        if (decoded) {
          request({
            method: 'HEAD',
            uri: config.security.hashValidationURI,
            timeout: config.security.hashValidationTimeout,
            headers: {
              user: decoded.userId,
              hash: decoded.hash
            }
          }, function(err, response, body) {
              if (err || response.statusCode !== 200) {
                res.status(401)
                  .send(
                    {error:
                      'O token de autenticação utilizado está vencido ou ' +
                      'inválido.'});
              } else {
                req.token = {
                  userId: decoded.userId,
                  hash: decoded.hash
                };
                next();
              }
            });
        } else if (err) {
          console.log(err);
          res.status(401)
            .send(
              {error:
                'O token de autenticação utilizado está vencido ou inválido.'});
        } else {
          res.status(401)
            .send(
              {error:
                'O token de autenticação utilizado está vencido ou inválido.'});
        }
      });
  } else {
    if (req.connection.remoteAddress === '127.0.0.1' ||
        req.connection.remoteAddress === '::ffff:127.0.0.1' ||
        req.connection.remoteAddress === '::1') {
      console.log('Security Module - Local connection: ' +
        req.connection.remoteAddress);
      next();
    } else {
      res.status(401)
        .send({error: 'Token vencido ou inválido.'});
    }
  }
};
