const express = require ('express');
const db = require('../config/db');
const bodyParser = require ('body-parser');

const app = express();

app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin',
                '*');
  res.setHeader('Access-Control-Allow-Methods',
                'GET, POST, HEAD');
  res.setHeader('Access-Control-Allow-Headers',
                'X-Requested-With, content-type, Authorization');
  next();
});

module.exports = app;
