module.exports = {
  VALIDATION_ERROR_NAME: 'ValidationError',

  INTERNAL_SERVER_ERROR: 'internalServerError',
  UNAUTHORIZED: 'unauthorized',
  OK: 'OK',
  NOT_FOUND: 'notFound',
  BAD_REQUEST: 'badRequest'
};
