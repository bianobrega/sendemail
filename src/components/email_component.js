const nodemailer = require ('nodemailer');
const config = require('../../config/config');

exports.sendEmail = function(email) {
  return new Promise((resolve, reject) => {
    const transport = nodemailer.createTransport({
      host: config.component.emailManager.contact.host,
      port: config.component.emailManager.contact.port,
      secure: config.component.emailManager.contact.secure,
      auth: {
        user: config.component.emailManager.contact.user,
        pass: config.component.emailManager.contact.password
      }
    });

    const request = {
      from: email.from,
      to: email.to,
      subject: email.subject,
      html: email.body
    };

    transport.sendMail(request, function(err) {
      if (err) {
        console.log(err);
        reject(err);
      } else {
        resolve();
      }
    });

  });
};
