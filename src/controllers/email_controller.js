const emailComponent = require('../components/email_component');
const emailService = require('../services/email_service');
const httpCodes = require('../commons/http_codes');
const constants = require('../commons/constants');
const messages = require('../../language/messages');
const config = require('../../config/config');

exports.sendEmail = function(req, res) {
  try {
    const email = {
      user: req.body.user,
      from: config.component.emailManager.contact.user,
      to: req.body.to,
      subject: req.body.subject,
      body: req.body.body
    };

    emailComponent.sendEmail(email)
      .then(() => {
        email.sent = new Date();
        emailService.createAndSave(email)
          .then(() => {
            res.status(httpCodes.OK)
              .send({message: messages.controller.email.sucess});

          })
          .catch((err) => {
            if (err.code === constants.BAD_REQUEST) {
              res.status(httpCodes.BAD_REQUEST)
                .send(err.messages);

            } else {
              res.status(httpCodes.INTERNAL_SERVER_ERROR)
                .send(err.messages);
            }
          });
      })
      .catch((err) => {
        console.log(err);
        emailService.createAndSave(email)
          .then(() => {
            res.status(httpCodes.OK)
              .send({message: messages.controller.email.sucess});

          })
          .catch((err) => {
            if (err.code === constants.BAD_REQUEST) {
              res.status(httpCodes.BAD_REQUEST)
                .send(err.messages);

            } else {
              res.status(httpCodes.INTERNAL_SERVER_ERROR)
                .send(err.messages);
            }
          });

      });
  } catch (e) {
    console.log(e);
    res.status(httpCodes.INTERNAL_SERVER_ERROR)
      .send({error: messages.default.internalServerError});
  }
};
