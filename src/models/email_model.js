const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const messages = require('../../language/messages');

const EmailSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    required: [true, messages.model.email.userIdRequired],
  },
  created: {
    type: Date,
    default: Date.now,
  },
  sent: {
    type: Date,
  },
  from: {
    type: String,
    required: [true, messages.model.email.fromRequired],
    validate: {
      validator: (from) => from.length < 200,
      message: messages.model.email.fromMaxLength,
    },
  },
  to: {
    type: String,
    required: [true, messages.model.email.toRequired],
    validate: {
      validator: (to) => to.length < 200,
      message: messages.model.email.toMaxLength,
    },
  },
  subject: {
    type: String,
    required: [true, messages.model.email.subjectRequired],
    validate: {
      validator: (subject) => subject.length < 100,
      message: messages.model.email.subjectMaxLength,
    },
  }
});

EmailSchema.virtual('body').get(function() {
  return this._body;
});
EmailSchema.virtual('body').set(function(body) {
  this._body = body;
});

const Email = mongoose.model('email', EmailSchema);

module.exports = Email;
