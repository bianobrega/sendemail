var emailController = require ('../controllers/email_controller');

module.exports = function(app) {
  app.post('/api/email', emailController.sendEmail);
};
