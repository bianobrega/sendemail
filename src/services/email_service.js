const Email = require ('../models/email_model');
const constants = require('../commons/constants');
const messages = require('../../language/messages');
const config = require('../../config/config');

exports.createAndSave = function(email) {
  return new Promise((resolve, reject) => {
    let errors;
		try {
      const emailModel = new Email(email);

      emailModel.save()
        .then(() => {
          resolve();
        })
        .catch((err) => {
          if (err.name === constants.VALIDATION_ERROR_NAME) {
          const errors = Object.keys(err.errors).map((errorField) => {
              return {error: err.errors[errorField].message};
            });
          errors = {
          	code: constants.BAD_REQUEST,
          	messages: errors
          }
          reject(errors);
        } else {
          console.log(err);
          errors = {
          	code: constants.INTERNAL_SERVER_ERROR,
          	messages: {errors: [{error: messages.controller.email.error}]}
          }
        }

        });
    } catch (err) {
      errors = {
        code: constants.INTERNAL_SERVER_ERROR,
        messages: {errors: [{error: messages.controller.email.error}]}
      }
    }
  });
};
